# Service Name : lf_syndication_store

  - The purpose of syndication store is to store all the syndication call data request/response into database.
  - This will be event based, which will be raised by syndication services.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work : 
	https://docs.microsoft.com/en-us/dotnet/core/
	https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
 ### Installing

 - Clone source using below git command or source tree
			git clone <repository_path>
	And go to respective branch for ex develop
			git fetch && git checkout develop	
 - Open command prompt and goto project directory.
 -  Build solution by command : 
		dotnet build <solution_file>
 - Run Solution by command
		dotnet run --project <project path>|
		eg. dotnet run --project  "src\LendFoundry.SyndicationStore.Api\LendFoundry.SyndicationStore.Api.csproj"
		
## Running the tests

- There is cover.sh file in solution for running tests and code coverage.
- To start the test 
			open cmd 
			goto project directory
			type command cover.sh and enter			
- you can see all tests result on cmd as well as all report of testing code coverage.
- also can find report file after testing completed from above command. 
			At path of solution folder : "artifacts\coverage\report\coverage_report\index.html"

###  What these tests test and why

- We are using xUnit testing and implementing for below projects.
	Api
	Client
	Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  syndication-store .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/syndication-store
	* Sample configurations :
	```
    {
        // Events to be added which syndication service raises. Based on this events it will added into database.
		"Events": [
            {
                "EventName": "SyndicationCalledEvent"
            }
        ],
        "dependencies": {
	    		"lookup": <lookup service URL>,
	    		"document_manager": <document manager service URL>,
	    		"template_manager": <template manager service URL>,
	    },
	    "ConnectionString": <database connection string>,
	    "Database": "syndication-store"
    }
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by syndication-store service

- Database : MongoDB
- Collections : syndication-store

## External Services called

NA

## Environment Variables used by syndication-store service

- Environment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different environment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting environment variable in launchSettings.json.
- Below are the environment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\<api_project>\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "syndication-store",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog