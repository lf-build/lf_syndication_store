﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Services;
using LendFoundry.SyndicationStore.Events;
using LendFoundry.SyndicationStore.Tests.Fake;
using Moq;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace LendFoundry.SyndicationStore.Tests
{
    public class SyndicationStoreServiceTests : InMemoryObjects
    {
        [Fact]
        public void Get()
        {
            // arrange
            var repository = new FakeRepository();
            var listOfEntries = new[] { InMemorySyndicationEntry, InMemorySyndicationEntry, InMemorySyndicationEntry }
            .Select(syndicationEntry =>
            {
                return syndicationEntry;
            }).ToList();

            listOfEntries.ForEach(repository.Add);

            var testObject = listOfEntries.FirstOrDefault();
            var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);

            // action
            var result = service.Get(testObject.Id);

            // assert
            Assert.NotNull(result);
            Assert.Equal(testObject.Id, result.Id);
            Assert.NotEmpty(result.Name);
            Assert.NotEmpty(result.EntityId);
            Assert.NotEmpty(result.EntityType);
        }

        [Fact]
        public void Get_When_ExecutionFailed()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.Get(It.IsAny<string>())).ReturnsAsync((SyndicationEntry)null);

                // action
                Service.Get(Guid.NewGuid().ToString("N"));

                // assert
                Repository.Verify(x => x.Get(It.IsAny<string>()), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public void Get_When_HasNo_Id()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.Get(It.IsAny<string>()));

                // action
                Service.Get(string.Empty);

                // assert
                Repository.Verify(x => x.Get(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }


        [Fact]
        public void GetEntriesByName()
        {
            // arrange
            var repository = new FakeRepository();
            var listOfEntries = new[] { InMemorySyndicationEntry, InMemorySyndicationEntry, InMemorySyndicationEntry }
            .Select((syndicationEntry, i) =>
            {
                i++;
                syndicationEntry.Data = new { Count = i };
                return syndicationEntry;
            }).ToList();

            listOfEntries.ForEach(repository.Add);
            var testObject = listOfEntries.FirstOrDefault();
            TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });

            var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);

            // action
            var result = service.GetEntries(testObject.Name, 1, 1);

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(testObject.Id, result.FirstOrDefault().Id);
            Assert.Equal(result.Count(), listOfEntries.Count);
        }

        [Fact]
        public void GetEntriesByName_ExecutionFailed()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());

                // action
                Service.GetEntries(Guid.NewGuid().ToString("N"), 1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public void GetEntriesByName_When_HasNo_Name()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1));

                // action
                Service.GetEntries(string.Empty, 1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.Never);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public void GetEntriesWithReferenceNumberWhenSuccess()
        {
            // arrange
            var repository = new FakeRepository();
            var listOfEntries = new[] { InMemorySyndicationEntry, InMemorySyndicationEntry, InMemorySyndicationEntry }
            .Select((syndicationEntry, i) =>
            {
                i++;
                syndicationEntry.Data = new { Count = i };
                return syndicationEntry;
            }).ToList();

            listOfEntries.ForEach(repository.Add);
            var testObject = listOfEntries.FirstOrDefault();

            LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });

            LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
            var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);



            // action
            var result = service.GetEntries(testObject.EntityType, testObject.EntityId, "ref001");

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(testObject.Id, result.FirstOrDefault().Id);
            Assert.Equal(testObject.EntityType, result.FirstOrDefault().EntityType);
            Assert.Equal(result.Count(), listOfEntries.Count);
        }
        [Fact]
        public void GetEntriesWithReferenceNumberWhen_EntityTypeNull()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries(null, "entityId", "ref001");

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesWithReferenceNumberWhen_EntityIdNull()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("entityType", "", "ref001");

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesWithReferenceNumberWhen_ReferenceNumberNull()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("entityType", "entityId", "");

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public void GetEntriesWithReferenceNumberWhen_NotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("entityType", "entityId", "111");

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntity()
        {
            // arrange
            var repository = new FakeRepository();
            var listOfEntries = new[] { InMemorySyndicationEntry, InMemorySyndicationEntry, InMemorySyndicationEntry }
            .Select((syndicationEntry, i) =>
            {
                i++;
                syndicationEntry.Data = new { Count = i };
                return syndicationEntry;
            }).ToList();

            listOfEntries.ForEach(repository.Add);
            var testObject = listOfEntries.FirstOrDefault();

            LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });

            LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
            var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);



            // action
            var result = service.GetEntries(testObject.EntityType, testObject.EntityId, 1, 1);

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(testObject.Id, result.FirstOrDefault().Id);
            Assert.Equal(testObject.EntityType, result.FirstOrDefault().EntityType);
            Assert.Equal(result.Count(), listOfEntries.Count);
        }
        [Fact]
        public void GetEntriesByEntity_When_EntityIdNull()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", null, 1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }


        [Fact]
        public void GetEntriesByEntity_When_ExecutionFailed()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", "EntityId", 1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public void GetEntriesByEntity_When_SkipLessThanZero()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 0, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", -1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntity_When_TakeLessThanZero()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", 1, -1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }


        [Fact]
        public void GetEntriesByEntityWithId_When_SkipLessThanZero()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", "EntityId", -1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntityWithId_When_TakeLessThanZero()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", "EntityId", 1, -1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntityWithEntityId_When_TotalRecordsGreaterThanZero()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                Repository.Setup(x => x.Count(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>())).Returns(5);
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", "EntityId", 5, 5);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntity_When_TotalRecordsGreaterThanZero()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1)).ReturnsAsync(new List<ISyndicationEntry>());
                Repository.Setup(x => x.Count(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>())).Returns(5);
                LookupService.Setup(c => c.GetLookupEntries(It.IsAny<string>())).Returns(new Dictionary<string, string> { { "EntityType", "EntityType" } });
                LookupService.Setup(c => c.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
                // action
                Service.GetEntries("EntityType", 5, 5);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.AtLeastOnce);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }
        [Fact]
        public void GetEntriesByEntity_When_HasNo_Entity()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Repository.Setup(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1));

                // action
                Service.GetEntries(string.Empty, string.Empty, 1, 1);

                // assert
                Repository.Verify(x => x.All(It.IsAny<Expression<Func<ISyndicationEntry, bool>>>(), 1, 1), Times.Never);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void ProcessEvent()
        {
            // arrange
            TenantTime.Setup(x => x.Convert(It.IsAny<DateTimeOffset>())).Returns(new DateTimeOffset(DateTime.Now));
            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
            var repository = new FakeRepository();
            var eventInfo = new EventInfo
            {
                Data = new SyndicationCalledEvent
                {
                    Data = new { Test = "Fake" },
                    EntityId = "EntityId",
                    EntityType = "EntityType",
                    Name = Settings.ServiceName
                },
                Name = "eventInfo",
                //EntityType = "EntityType",
                //EntityId = "EntityId",
                TenantId = TenantId,
                Time = new Foundation.Date.TimeBucket(DateTime.Now)
            };
            TenantService.Setup(p => p.Current).Returns(new Tenant.Client.TenantInfo { Id = "my-tenant", Name = "my-tenant", IsActive = true });
            var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);

            // action
            service.ProcessEvent(eventInfo);
            var result = service.GetEntries(Settings.ServiceName, 1, 1);

            // assert
            Assert.NotNull(result);
            Assert.Equal(Settings.ServiceName, result.FirstOrDefault().Name);
            Assert.NotEmpty(result.FirstOrDefault().Name);
            EventHubClient.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        [Fact]
        public void ProcessEvent_When_EventNull()
        {


           
                // arrange
                TenantTime.Setup(x => x.Convert(It.IsAny<DateTimeOffset>())).Returns(new DateTimeOffset(DateTime.Now));
                EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                var repository = new FakeRepository();
                EventInfo eventInfo = null;

                var service = new SyndicationStoreService(repository, Logger.Object, EventHubClient.Object, TenantTime.Object, LookupService.Object, TenantService.Object);

                // action
                service.ProcessEvent(eventInfo);

                // assert
                EventHubClient.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.Never);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            
        }
    }
}