﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;

namespace LendFoundry.SyndicationStore.Tests
{
    public class InMemoryObjects
    {
        protected string TenantId { get; } = "my-tenant";

        // configuration
        protected Mock<IConfigurationServiceFactory> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory>();
        //protected Mock<IConfigurationService<Dictionary<string, List<SyndicationStoreEventConfiguration>>>> ConfigurationService { get; } = new Mock<IConfigurationService<Dictionary<string, List<SyndicationStoreEventConfiguration>>>>();

        // token handler
        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        // eventhub
        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();
        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        // logger
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        // tenant 
        protected Mock<ITenantServiceFactory> TenantServiceFactory { get; } = new Mock<ITenantServiceFactory>();
        protected Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        // tenantTime
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        // lookup
        protected Mock<ILookupServiceFactory> LookupServiceFactory { get; } = new Mock<ILookupServiceFactory>();
        protected Mock<ILookupService> LookupService { get; } = new Mock<ILookupService>();

        // repository
        protected Mock<ISyndicationStoreRepositoryFactory> SyndicationStoreRepositoryFactory { get; } = new Mock<ISyndicationStoreRepositoryFactory>();
        protected Mock<ISyndicationStoreRepository> Repository { get; } = new Mock<ISyndicationStoreRepository>();

        // service
        protected Mock<ISyndicationStoreServiceFactory> SyndicationStoreServiceFactory { get; } = new Mock<ISyndicationStoreServiceFactory>();
        protected Mock<ISyndicationStoreServiceExtended> SyndicationStoreService { get; } = new Mock<ISyndicationStoreServiceExtended>();

        // auxiliar
        protected ISyndicationStoreListener SyndicationStoreListener => new SyndicationStoreListener
        (
            SyndicationStoreServiceFactory.Object, ConfigurationServiceFactory.Object, TokenHandler.Object,
            EventHubClientFactory.Object, LoggerFactory.Object, TenantServiceFactory.Object
        );

        protected ISyndicationStoreServiceExtended Service => new SyndicationStoreService
        (
            Repository.Object, 
            Logger.Object, 
            EventHubClient.Object,
            TenantTime.Object,
            LookupService.Object,
            TenantService.Object
        );

        protected ISyndicationEntry InMemorySyndicationEntry => new SyndicationEntry
        {
            TenantId = TenantId,
            EntityId = "entityid",
            EntityType = "entitytype",
            Data = null,
            Name = "EventName",
            Time = new TimeBucket(new DateTimeOffset(DateTime.Now)),
            ReferenceNumber="ref001"
        };

        protected IEnumerable<TenantInfo> InMemoryTenants => new[]
        {
            new TenantInfo { Id = "my-tenant" },
        };

        
    }
}