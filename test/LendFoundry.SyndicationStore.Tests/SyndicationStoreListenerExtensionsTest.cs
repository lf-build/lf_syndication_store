﻿
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LendFoundry.SyndicationStore.Tests
{
   public class SyndicationStoreListenerExtensionsTest
    {
        [Fact]
        public void UseSyndicationStoreListener_InvalidOperation()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var listener = new SyndicationStoreListener
                (
                    Mock.Of<ISyndicationStoreServiceFactory>(),
                 Mock.Of<IConfigurationServiceFactory>(),
                   Mock.Of<ITokenHandler>(),
                   Mock.Of<IEventHubClientFactory>(),
                   Mock.Of<ILoggerFactory>(),
                   Mock.Of<ITenantServiceFactory>()
                );

                var applicationBuilder = new Mock<IApplicationBuilder>();
                var serviceProvider = new Mock<IServiceProvider>();

                serviceProvider.Setup(x => x.GetRequiredService<ISyndicationStoreListener>())
                    .Returns(listener);

                applicationBuilder.Setup(x => x.ApplicationServices)
                    .Returns(serviceProvider.Object);

                SyndicationStoreListenerExtensions.UseSyndicationStoreListener(applicationBuilder.Object);
            });
        }

        public interface IServiceProvider : System.IServiceProvider
        {
            T GetService<T>() where T : class;

            T GetRequiredService<T>() where T : class;
        }
    }

   
}
