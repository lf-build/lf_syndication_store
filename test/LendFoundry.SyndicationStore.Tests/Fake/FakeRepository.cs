﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.SyndicationStore.Tests.Fake
{
    public class FakeRepository : ISyndicationStoreRepository
    {
        public List<ISyndicationEntry> InMemoryData { get; set; } = new List<ISyndicationEntry>();

        public void Add(ISyndicationEntry item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public Task<IEnumerable<ISyndicationEntry>> All(Expression<Func<ISyndicationEntry, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run( () => 
            {
                return InMemoryData.AsQueryable().Where(query).AsEnumerable();
            });
        }

        public int Count(Expression<Func<ISyndicationEntry, bool>> query)
        {
            return InMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<ISyndicationEntry> Get(string id)
        {
            return Task.Run(() => 
            {
                return InMemoryData.FirstOrDefault(s => s.Id == id);
            });
        }

        public void Remove(ISyndicationEntry item)
        {
            throw new NotImplementedException();
        }

        public void Update(ISyndicationEntry item)
        {
            throw new NotImplementedException();
        }
    }
}