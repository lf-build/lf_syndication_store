﻿using LendFoundry.SyndicationStore.Api.Controllers;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.SyndicationStore.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<ISyndicationStoreService> SyndicationStore { get; }
      
        private ApiController Api { get; }

        public ApiControllerTests()
        {
            SyndicationStore = new Mock<ISyndicationStoreService>();
          
            Api = new ApiController(SyndicationStore.Object,Mock.Of<ILogger>());
        }

       

        [Fact]
        public  void Controller_Get_Returns_Result_OnSuccess()
        {
            SyndicationStore.Setup(x => x.Get(It.IsAny<string>()))
                .Returns(new SyndicationEntry());

            var result = (OkObjectResult) Api.Get(It.IsAny<string>());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<SyndicationEntry>(result.Value);
        }

        [Fact]
        public void Controller_Get_Returns_ErrorResult()
        {
            SyndicationStore.Setup(x => x.Get(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(It.IsAny<string>()));

            var result = (ErrorResult)Api.Get(It.IsAny<string>());

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
          
        }

        [Fact]
        public void Controller_GetEntries_Returns_Result_OnSuccess()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), 0, 0))
                .Returns(new List<SyndicationEntry>());

            var result = (OkObjectResult)Api.GetEntries(It.IsAny<string>(), 0, 0);

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<SyndicationEntry>>(result.Value);
        }
        [Fact]
        public void Controller_GetEntries_Returns_ErrorResult()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), 0, 0))
               .Throws(new InvalidArgumentException(It.IsAny<string>()));

            var result = (ErrorResult)Api.GetEntries(It.IsAny<string>(), 0, 0);

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
          
        }
        [Fact]
        public void Controller_GetActivities_Returns_Result_OnSuccess()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), It.IsAny<string>(),0,0))
                .Returns(new List<SyndicationEntry>());

            var result = (OkObjectResult)Api.GetActivities(It.IsAny<string>(),It.IsAny<string>(), 0, 0);

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<SyndicationEntry>>(result.Value);
        }
        [Fact]
        public void Controller_GetActivities_Returns_ErrorResult()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), It.IsAny<string>(), 0, 0))
               .Throws(new InvalidArgumentException(It.IsAny<string>()));

            var result = (ErrorResult)Api.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 0, 0);

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public void Controller_GetActivitiesWithReferenceNumber_Returns_Result_OnSuccess()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<SyndicationEntry>());

            var result = (OkObjectResult)Api.GetActivities(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<SyndicationEntry>>(result.Value);
        }
        [Fact]
        public void Controller_GetActivitiesWithReferenceNumber_Returns_ErrorResult()
        {
            SyndicationStore.Setup(x => x.GetEntries(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
               .Throws(new InvalidArgumentException(It.IsAny<string>()));

            var result = (ErrorResult)Api.GetActivities(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>());

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);

        }
    }
}