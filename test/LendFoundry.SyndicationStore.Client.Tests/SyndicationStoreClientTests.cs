﻿
using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.SyndicationStore.Client.Tests
{
    public class SyndicationStoreClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; }
        private ISyndicationStoreService Client { get; }
        private IRestRequest Request { get; set; }

        public SyndicationStoreClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new SyndicationStoreClient(ServiceClient.Object);
        }

        [Fact]
        public  void Client_Get()
        {
            ServiceClient.Setup(s => s.Execute<SyndicationEntry>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(new SyndicationEntry() { Id="123", EntityId = "123" } );

            var result =  Client.Get("123");

            ServiceClient.Verify(x => x.Execute<SyndicationEntry>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/by/id/{id}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }
        [Fact]
        public void Client_GetEntries()
        {
            ServiceClient.Setup(s => s.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(new List<SyndicationEntry>() { new SyndicationEntry { Id = "123", EntityId = "123" } });

            var result = Client.GetEntries(It.IsAny<string>(),0,0);

            ServiceClient.Verify(x => x.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/by/name/{name}/{skip?}/{take?}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);



        }
        [Fact]
        public void Client_GetEntriesWithEntityId()
        {
            ServiceClient.Setup(s => s.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(new List<SyndicationEntry>() { new SyndicationEntry { Id = "123", EntityId = "123" } });

            var result = Client.GetEntries(It.IsAny<string>(), It.IsAny<string>(), 0, 0);

            ServiceClient.Verify(x => x.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/by/entity/{entityType}/{entityId}/{skip?}/{take?}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);



        }
        [Fact]
        public void Client_GetEntriesWithEntityIdAndReferenceNumber()
        {
            ServiceClient.Setup(s => s.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(new List<SyndicationEntry>() { new SyndicationEntry { Id = "123", EntityId = "123" } });

            var result = Client.GetEntries(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());

            ServiceClient.Verify(x => x.Execute<List<SyndicationEntry>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/by/entity/referencenumber/{entityType}/{entityId}/{referenceNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);



        }
        
    }
}