using LendFoundry.Foundation.Persistence;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreRepository : IRepository<ISyndicationEntry>
    {
      
    }
}