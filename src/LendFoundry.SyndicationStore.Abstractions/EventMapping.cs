﻿namespace LendFoundry.SyndicationStore
{
    public class EventMapping
    {
        public string EventName { get; set; }

        public string SyndicationName { get; set; }
    }
}
