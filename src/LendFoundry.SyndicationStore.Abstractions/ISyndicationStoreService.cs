using System.Collections.Generic;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreService
    {
        ISyndicationEntry Get(string id);

        IEnumerable<ISyndicationEntry> GetEntries(string name, int? skip, int? take);

        IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, int? skip, int? take);

        IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, string referenceNumber);

        IEnumerable<ISyndicationEntry> GetEntriesByEventName(string entityType, string entityId, string eventName);
    }
}