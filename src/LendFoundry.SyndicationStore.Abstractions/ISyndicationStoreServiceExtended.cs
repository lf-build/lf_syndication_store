using LendFoundry.EventHub;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreServiceExtended : ISyndicationStoreService
    {
        void ProcessEvent(EventInfo @event);
    }
}