﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.SyndicationStore
{
    public class SyndicationStoreConfiguration : IDependencyConfiguration
    {
        public List<EventMapping> Events { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}