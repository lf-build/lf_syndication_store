﻿using System;

namespace LendFoundry.SyndicationStore.Events
{
    public class SyndicationCalledEvent : SyndicationBasedEvent
    {
        public DateTimeOffset? Expiration { get; set; }
    }
}