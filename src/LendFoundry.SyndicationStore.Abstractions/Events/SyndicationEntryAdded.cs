﻿using System;

namespace LendFoundry.SyndicationStore.Events
{
    public class SyndicationEntryAdded : SyndicationBasedEvent
    {
        public SyndicationEntryAdded() { }

        public SyndicationEntryAdded(ISyndicationEntry syndicationEntry)
        {
            EntryId = syndicationEntry.Id;
            EntityId = syndicationEntry.EntityId;
            EntityType = syndicationEntry.EntityType;

            if (syndicationEntry.Expiration != null)
                Expiration = syndicationEntry.Expiration.Time;

            Request = syndicationEntry.Request;
            Response = syndicationEntry.Response;
            ReferenceNumber = syndicationEntry.ReferenceNumber;
            Data = syndicationEntry.Data;
            Name = syndicationEntry.Name;

            if (syndicationEntry.Time != null)
                Time = syndicationEntry.Time.Time;
        }

        public string EntryId { get; set; }

        public DateTimeOffset? Expiration { get; set; }

        public DateTimeOffset Time { get; set; }
    }
}