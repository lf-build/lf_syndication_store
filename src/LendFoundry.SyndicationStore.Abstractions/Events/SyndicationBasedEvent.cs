﻿namespace LendFoundry.SyndicationStore.Events
{
    public class SyndicationBasedEvent
    {
        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public object Data { get; set; }

        public string Name { get; set; }

        public string ReferenceNumber { get; set; }

        public object Request { get; set; }

        public object Response { get; set; }
    }
}