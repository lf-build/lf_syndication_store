﻿using System;

namespace LendFoundry.SyndicationStore
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "syndication-store";
    }
}