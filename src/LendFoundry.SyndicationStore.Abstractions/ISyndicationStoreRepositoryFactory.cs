using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreRepositoryFactory
    {
        ISyndicationStoreRepository Create(ITokenReader reader);
    }
}