﻿
namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreListener
    {
        void Start();
    }
}