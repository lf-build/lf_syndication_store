﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationEntry : IAggregate
    {
        string EntityId { get; set; }

        string EntityType { get; set; }

        TimeBucket Expiration { get; set; }

        object Data { get; set; }

        object Request { get; set; }

        object Response { get; set; }

        string Name { get; set; }

        TimeBucket Time { get; set; }

        string ReferenceNumber { get; set; }

        string EventName { get; set; }

        string Username { get; set; }
        string IpAddress { get; set; }
    }
}