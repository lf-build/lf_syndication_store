﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.SyndicationStore
{
    public class SyndicationEntry : Aggregate, ISyndicationEntry
    {
        public SyndicationEntry() { }

        public SyndicationEntry(SyndicationCalledEvent syndicationCalledEvent)
        {
            if (syndicationCalledEvent != null)
            {
                EntityId = syndicationCalledEvent.EntityId;
                EntityType = syndicationCalledEvent.EntityType;
                Request = syndicationCalledEvent.Request;
                Response = syndicationCalledEvent.Response;
                Data = syndicationCalledEvent.Data;
                Name = syndicationCalledEvent.Name;
                ReferenceNumber = syndicationCalledEvent.ReferenceNumber;
            }
        }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public TimeBucket Expiration { get; set; }

        public object Data { get; set; }

        public object Request { get; set; }

        public object Response { get; set; }

        public string Name { get; set; }

        public TimeBucket Time { get; set; }

        public string ReferenceNumber { get; set; }

        public string EventName { get; set; }

        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
}