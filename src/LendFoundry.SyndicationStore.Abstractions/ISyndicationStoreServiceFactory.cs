using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationStore
{
    public interface ISyndicationStoreServiceFactory
    {
        ISyndicationStoreServiceExtended Create(StaticTokenReader reader, ILogger logger);
    }
}