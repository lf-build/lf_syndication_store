﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using LendFoundry.Foundation.Listener;
using System.Collections.Generic;

namespace LendFoundry.SyndicationStore
{
    public class SyndicationStoreListener : ListenerBase, ISyndicationStoreListener
    {
        public SyndicationStoreListener
        (
            ISyndicationStoreServiceFactory syndicationStoreFactory,
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory
        ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            SyndicationStoreFactory = syndicationStoreFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        private ISyndicationStoreServiceFactory SyndicationStoreFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var eventhub = EventHubFactory.Create(reader);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var syndicationStoreService = SyndicationStoreFactory.Create(reader, logger);
            var configuration = ConfigurationFactory.Create<SyndicationStoreConfiguration>(Settings.ServiceName, reader).Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                var uniqueEvents = configuration.Events.Select(s => s.EventName).Distinct().ToList();
                return uniqueEvents;
            }
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var eventhub = EventHubFactory.Create(reader);
            var syndicationStoreService = SyndicationStoreFactory.Create(reader, logger);
            var configuration = ConfigurationFactory.Create<SyndicationStoreConfiguration>(Settings.ServiceName, reader).Get();

            if (configuration == null)
            {
                logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                return null;
            }
            else
            {
                var uniqueEvents = configuration.Events.Select(s => s.EventName).Distinct().ToList();
                uniqueEvents.ForEach(eventName =>
                               {
                                   eventhub.On(eventName, syndicationStoreService.ProcessEvent);
                                   logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                               });
                return uniqueEvents;
            }
        }
    }
}