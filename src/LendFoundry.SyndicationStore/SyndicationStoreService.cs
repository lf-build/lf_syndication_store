﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.SyndicationStore.Events;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.SyndicationStore
{
    public class SyndicationStoreService : ISyndicationStoreServiceExtended
    {
        public SyndicationStoreService
        (
            ISyndicationStoreRepository repository, 
            ILogger logger, 
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            ILookupService lookup,
            ITenantService tenant
        )
        {
            Repository = repository;
            Logger = logger;
            EventHubClient = eventHubClient;
            TenantTime = tenantTime;
            Lookup = lookup;
            Tenant = tenant;
        }

        private ISyndicationStoreRepository Repository { get; }

        private ILogger Logger { get; }

        private IEventHubClient EventHubClient { get; }

        private ITenantTime TenantTime { get; }
        private ILookupService Lookup { get; }
        private ITenantService Tenant { get; }

        public ISyndicationEntry Get(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new InvalidArgumentException($"#{nameof(id)} cannot be null", nameof(id));

                var entry = Repository.Get(id).Result;
                if (entry == null)
                    throw new NotFoundException($"Syndication entry was not found");

                return entry;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method Get({id}) raised an error:{ex.Message}, \nError:", ex);
                throw;
            }
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string name, int? skip, int? take)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    throw new InvalidArgumentException($"#{nameof(name)} cannot be null", nameof(name));

                if (skip < 0)
                    throw new InvalidArgumentException($"#{nameof(skip)} value is invalid", nameof(skip));

                if (take < 0)
                    throw new InvalidArgumentException($"#{nameof(take)} value is invalid", nameof(take));

                Expression<Func<ISyndicationEntry, bool>> query = q => 
                q.TenantId == Tenant.Current.Id && 
                q.Name.ToLower().StartsWith(name.ToLower()) || 
                q.Name.ToLower().EndsWith(name.ToLower());
                
               
                var totalRecords = Repository.Count(query);
              
                var entries = Repository.All(query, skip, take).Result.OrderBy(s => s.Time.Time).ToList();

                if ((totalRecords > entries.Count) && (skip.HasValue && skip.Value >= totalRecords))
                    throw new NotFoundException($"Name: #{name} has only {totalRecords} syndication entries");

                if (!entries.Any())
                    throw new NotFoundException($"Name: #{name} has only {totalRecords} syndication entries");

                Logger.Info($"SyndicationStore list search requested by Name: #{name} with #{entries.Count} results");

                return entries;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetEntries({name}, {skip}, {take}) raised an error:{ex.Message}, \nError:", ex);
                throw;
            }
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, int? skip, int? take)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new InvalidArgumentException($"#{nameof(entityType)} cannot be null", nameof(entityType));

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new InvalidArgumentException($"#{nameof(entityId)} cannot be null", nameof(entityId));

                if (skip < 0)
                    throw new InvalidArgumentException($"#{nameof(skip)} value is invalid", nameof(skip));

                if (take < 0)
                    throw new InvalidArgumentException($"#{nameof(take)} value is invalid", nameof(take));

                entityType = EnsureEntityType(entityType);

                Expression<Func<ISyndicationEntry, bool>> query = q =>
                q.TenantId == Tenant.Current.Id &&
                q.EntityType == entityType &&
                q.EntityId == entityId;

                var totalRecords = Repository.Count(query);
                var entries = Repository.All(query, skip, take).Result.OrderBy(s => s.Time.Time).ToList();

                if ((totalRecords > entries.Count) && (skip.HasValue && skip.Value >= totalRecords))
                    throw new NotFoundException($"EntityType: #{entityType}, EntityId: #{entityId} has only {totalRecords} syndication entries");

                if (!entries.Any())
                    throw new NotFoundException($"EntityType: #{entityType}, EntityId: #{entityId} has no syndication entries");

                Logger.Info($"SyndicationStore list search requested by EntityType: #{entityType}, Id: #{entityId} with #{entries.Count} results");

                return entries;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetEntries({entityType}, {entityId}, {skip}, {take}) raised an error:{ex.Message}, \nError:", ex);
                throw;
            }
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, string referenceNumber)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new InvalidArgumentException($"#{nameof(entityType)} cannot be null", nameof(entityType));

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new InvalidArgumentException($"#{nameof(entityId)} cannot be null", nameof(entityId));

                if (string.IsNullOrWhiteSpace(referenceNumber))
                    throw new InvalidArgumentException($"#{nameof(referenceNumber)} cannot be null", nameof(referenceNumber));

                entityType = EnsureEntityType(entityType);

                Expression<Func<ISyndicationEntry, bool>> query = q =>
                q.TenantId == Tenant.Current.Id &&
                q.EntityType == entityType &&
                q.EntityId == entityId &&
                q.ReferenceNumber == referenceNumber;

                var entries = Repository.All(query).Result.OrderBy(s => s.Time.Time).ToList();

                if (!entries.Any())
                    throw new NotFoundException($"EntityType: #{entityType}, EntityId: #{entityId}, ReferenceNumber : #{referenceNumber} has no syndication entries");

                Logger.Info($"SyndicationStore list search requested by EntityType: #{entityType}, Id: #{entityId} , ReferenceNumber : #{referenceNumber} with #{entries.ToList().Count} results");

                return entries;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetEntries({entityType}, {entityId}, {referenceNumber}) raised an error:{ex.Message}, \nError:", ex);
                throw;
            }
        }

        public IEnumerable<ISyndicationEntry> GetEntriesByEventName(string entityType, string entityId, string eventName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new InvalidArgumentException($"#{nameof(entityType)} cannot be null", nameof(entityType));

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new InvalidArgumentException($"#{nameof(entityId)} cannot be null", nameof(entityId));

                if (string.IsNullOrWhiteSpace(eventName))
                    throw new InvalidArgumentException($"#{nameof(eventName)} cannot be null", nameof(eventName));

                entityType = EnsureEntityType(entityType);

                Expression<Func<ISyndicationEntry, bool>> query = q =>
                q.TenantId == Tenant.Current.Id &&
                q.EntityType == entityType &&
                q.EntityId == entityId &&
                q.EventName == eventName;

                var entries = Repository.All(query).Result.OrderBy(s => s.Time.Time).ToList();

                if (!entries.Any())
                    throw new NotFoundException($"EntityType: #{entityType}, EntityId: #{entityId}, ReferenceNumber : #{eventName} has no syndication entries");

                Logger.Info($"SyndicationStore list search requested by EntityType: #{entityType}, Id: #{entityId} , ReferenceNumber : #{eventName} with #{entries.ToList().Count} results");

                return entries;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetEntries({entityType}, {entityId}, {eventName}) raised an error:{ex.Message}, \nError:", ex);
                throw;
            }
        }

        public void ProcessEvent(EventInfo @event)
        {
            try
            {
                if (@event == null)
                    throw new InvalidArgumentException($"#{nameof(@event)} cannot be null", nameof(@event));

                var syndicationCalledEvent = @event.Cast<SyndicationCalledEvent>();
                var syndicationEntry = new SyndicationEntry(syndicationCalledEvent);
                if (syndicationCalledEvent.Expiration.HasValue)
                    syndicationEntry.Expiration = new TimeBucket(TenantTime.Convert(syndicationCalledEvent.Expiration.Value));
                syndicationEntry.Data = syndicationCalledEvent.Data;
                if (syndicationEntry.EntityType != null)
                    syndicationEntry.EntityType = syndicationEntry.EntityType.ToLower();
                syndicationEntry.Time = @event.Time;
                syndicationEntry.EventName = @event.Name;
                syndicationEntry.Username = @event.UserName;
                syndicationEntry.IpAddress = @event.IpAddress;
                Repository.Add(syndicationEntry);
                Logger.Info($"The SyndicationEntry was saved in database");
                EventHubClient.Publish(nameof(SyndicationEntryAdded), new SyndicationEntryAdded(syndicationEntry));
            }
            catch (Exception ex)
            {
                Logger.Error($"The method ProcessEvent raised an error:{ex.Message}, Full Error:", ex, @event);

            }
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.Exist("entityTypes", entityType);

            if (!validEntityTypes)
                throw new ArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}