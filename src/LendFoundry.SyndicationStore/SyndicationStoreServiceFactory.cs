﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.SyndicationStore
{
    public class SyndicationStoreServiceFactory : ISyndicationStoreServiceFactory
    {
        public SyndicationStoreServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public ISyndicationStoreServiceExtended Create(StaticTokenReader reader, ILogger logger)
        {
            var repositoryFactory = Provider.GetService<ISyndicationStoreRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventhub = eventHubFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configuration = configurationServiceFactory.Create<SyndicationStoreConfiguration>(Settings.ServiceName, reader);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupServiceFactory.Create(reader);

            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);

            return new SyndicationStoreService(repository, logger, eventhub, tenantTime, lookupService, tenantService);
        }
    }
}