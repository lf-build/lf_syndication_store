﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.SyndicationStore
{
    public static class SyndicationStoreListenerExtensions
    {
        public static void UseSyndicationStoreListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<ISyndicationStoreListener>().Start();
        }
    }
}