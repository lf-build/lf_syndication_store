﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif
namespace LendFoundry.SyndicationStore.Api.Controllers
{
    /// <summary>
    /// Represents the api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents the constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(ISyndicationStoreService service, ILogger logger) : base(logger) 
        {
            
            Service = service;
        }

        private ISyndicationStoreService Service { get; }

        /// <summary>
        /// Get the data based on id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("/by/id/{id}")]
        [ProducesResponseType(typeof(ISyndicationEntry),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Get(string id)
        {
            return Execute(() =>
            {
                ISyndicationEntry entry = Service.Get(id);
                return Ok(entry);
            });
        }

        /// <summary>
        /// Get Entries by name.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet("/by/name/{name}/{skip?}/{take?}")]
        [ProducesResponseType(typeof(IEnumerable<ISyndicationEntry>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetEntries(string name, int? skip = default(int?), int? take = default(int?))
        {
            return Execute(() =>
            {
                IEnumerable<ISyndicationEntry> entries = Service.GetEntries(name, skip, take);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Get entries by entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet("/by/entity/{entityType}/{entityId}/{skip?}/{take?}")]
        [ProducesResponseType(typeof(IEnumerable<ISyndicationEntry>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetActivities(string entityType, string entityId, int? skip = default(int?), int? take = default(int?))
        {
            return Execute(() =>
            {
                IEnumerable<ISyndicationEntry> entries = Service.GetEntries(entityType, entityId, skip, take);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Get Entries by reference number
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="referenceNumber"></param>
        /// <returns></returns>
        [HttpGet("/by/entity/referencenumber/{entityType}/{entityId}/{referenceNumber}")]
        [ProducesResponseType(typeof(IEnumerable<ISyndicationEntry>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetActivities(string entityType, string entityId, string referenceNumber)
        {
            return Execute(() =>
            {
                return Ok(Service.GetEntries(entityType, entityId, referenceNumber));
            });
        }

        /// <summary>
        /// Get Entries by event name
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        [HttpGet("/by/entity/eventname/{entityType}/{entityId}/{eventName}")]
        [ProducesResponseType(typeof(IEnumerable<ISyndicationEntry>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetActivitiesByEventName(string entityType, string entityId, string eventName)
        {
            return Execute(() =>
            {
                return Ok(Service.GetEntriesByEventName(entityType, entityId, eventName));
            });
        }
    }
}