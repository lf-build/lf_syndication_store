﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace LendFoundry.SyndicationStore.Persistence
{
    public class SyndicationStoreRepository : MongoRepository<ISyndicationEntry, SyndicationEntry>, ISyndicationStoreRepository
    {
        static SyndicationStoreRepository()
        {
            
        }

        public SyndicationStoreRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "syndication-store")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(SyndicationEntry)))
            {
                BsonClassMap.RegisterClassMap<SyndicationEntry>(map =>
                {
                    map.AutoMap();
                    var type = typeof(SyndicationEntry);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                    map.MapMember(m => m.Response).SetSerializer(new BsonEncryptorWithBackwardCompatiblity<object, object>(encrypterService));
                    map.MapMember(m => m.Data).SetSerializer(new BsonEncryptorWithBackwardCompatiblity<object, object>(encrypterService));
                    map.MapMember(m => m.Request).SetSerializer(new BsonEncryptorWithBackwardCompatiblity<object, object>(encrypterService));
                    
                });
            }
            CreateIndexIfNotExists
            (
               indexName: "entityId",
               index: Builders<ISyndicationEntry>.IndexKeys.Ascending(i => i.EntityId)
            );

            CreateIndexIfNotExists
            (
               indexName: "entity-type-id",
               index: Builders<ISyndicationEntry>.IndexKeys.Ascending(i=>i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId)
            );
        }
    }
}