﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.SyndicationStore.Persistence
{
    public class SyndicationStoreRepositoryFactory : ISyndicationStoreRepositoryFactory
    {
        public SyndicationStoreRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ISyndicationStoreRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);

            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new SyndicationStoreRepository(tenantService, configuration, encryptionService);
        }
    }
}