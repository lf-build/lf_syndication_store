﻿using LendFoundry.Security.Encryption;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace LendFoundry.SyndicationStore.Persistence
{
    public class BsonEncryptorWithBackwardCompatiblity<TInterface, TConcrete> : IBsonSerializer<TInterface> where TConcrete : TInterface
    {
        private IEncryptionService EncrypterService { get; set; }
        private JsonSerializerSettings Settings { get; } = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public BsonEncryptorWithBackwardCompatiblity(IEncryptionService encrypterService)
        {
            EncrypterService = encrypterService;
        }

        public TInterface Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            if (!String.IsNullOrWhiteSpace(jsonString) && (jsonString[0] == '{' || jsonString[0] == '"'))
                return JsonConvert.DeserializeObject<TConcrete>(jsonString, Settings); ;
            return EncrypterService.Decrypt<TConcrete>(jsonString);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, TInterface value)
        {
            var valueAsJson = EncrypterService.Encrypt(value);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(TInterface);

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = EncrypterService.Encrypt(value);
            context.Writer.WriteString(valueAsJson);
        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            if (!String.IsNullOrWhiteSpace(jsonString) && (jsonString[0] == '{' || jsonString[0] == '"'))
                return JsonConvert.DeserializeObject<TConcrete>(jsonString, Settings); ;
            return EncrypterService.Decrypt<TConcrete>(jsonString);
        }
    }
}