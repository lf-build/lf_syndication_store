﻿
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;

namespace LendFoundry.SyndicationStore.Client
{
    public class SyndicationStoreClient : ISyndicationStoreService
    {
        public SyndicationStoreClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }


        public ISyndicationEntry Get(string id)
        {
            var request = new RestRequest("/by/id/{id}", Method.GET);
            request.AddUrlSegment(nameof(id), id);

            return Client.Execute<SyndicationEntry>(request);
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string name, int? skip, int? take)
        {
            var request = new RestRequest("/by/name/{name}/{skip?}/{take?}", Method.GET);
            request.AddUrlSegment(nameof(name), name);

            if (skip != null) request.AddUrlSegment(nameof(skip), skip.Value.ToString());
            if (take != null) request.AddUrlSegment(nameof(take), take.Value.ToString());

            return Client.Execute<List<SyndicationEntry>>(request);
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, int? skip, int? take)
        {
            var request = new RestRequest("/by/entity/{entityType}/{entityId}/{skip?}/{take?}", Method.GET);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);

            if (skip != null) request.AddUrlSegment(nameof(skip), skip.Value.ToString());
            if (take != null) request.AddUrlSegment(nameof(take), take.Value.ToString());

            return Client.Execute<List<SyndicationEntry>>(request);
        }

        public IEnumerable<ISyndicationEntry> GetEntries(string entityType, string entityId, string referenceNumber)
        {
            var request = new RestRequest("/by/entity/referencenumber/{entityType}/{entityId}/{referenceNumber}", Method.GET);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);

            return Client.Execute<List<SyndicationEntry>>(request);
        }

        public IEnumerable<ISyndicationEntry> GetEntriesByEventName(string entityType, string entityId, string eventName)
        {
            var request = new RestRequest("/by/entity/eventname/{entityType}/{entityId}/{eventName}", Method.GET);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);
            request.AddUrlSegment(nameof(eventName), eventName);

            return Client.Execute<List<SyndicationEntry>>(request);
        }
    }
}