﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.SyndicationStore.Client
{
    public class SyndicationStoreClientFactory : ISyndicationStoreClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public SyndicationStoreClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public SyndicationStoreClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }


        public ISyndicationStoreService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("syndication_store");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new SyndicationStoreClient(client);

        }
    }
}