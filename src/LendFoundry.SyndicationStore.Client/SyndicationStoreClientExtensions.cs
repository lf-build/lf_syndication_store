﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.SyndicationStore.Client
{
    public static class SyndicationStoreClientExtensions
    {
       

        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddSyndicationStoreClient(this IServiceCollection services, string endpoint, int port)
        {
            services.AddSingleton<ISyndicationStoreClientFactory>(p => new SyndicationStoreClientFactory(p, endpoint, port));
            services.AddSingleton(p => p.GetService<ISyndicationStoreClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSyndicationStoreClient(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ISyndicationStoreClientFactory>(p => new SyndicationStoreClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<ISyndicationStoreClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSyndicationStoreClient(this IServiceCollection services)
        {
            services.AddSingleton<ISyndicationStoreClientFactory>(p => new SyndicationStoreClientFactory(p));
            services.AddSingleton(p => p.GetService<ISyndicationStoreClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}