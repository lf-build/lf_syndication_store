﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationStore.Client
{
    public interface ISyndicationStoreClientFactory
    {
        ISyndicationStoreService Create(ITokenReader reader);
    }
}